<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
route::prefix('sampah')->group(function () {
    Route::get('/', 'SampahController@index')->name('sampah.index');
    Route::post('/store',  'SampahController@store')->name('sampah.store');
    Route::post('/update/{id}',  'SampahController@update')->name('sampah.update');
    Route::delete('/delete/{id}',  'SampahController@destroy')->name('sampah.delete');
});

route::prefix('nasabah')->group(function () {
    Route::get('/', 'NasabahController@index')->name('nasabah.index');
    Route::post('/store',  'NasabahController@store')->name('nasabah.store');
    Route::post('/update/{id}',  'NasabahController@update')->name('nasabah.update');
    Route::delete('/delete/{id}',  'NasabahController@destroy')->name('nasabah.delete');
});
route::prefix('petugas')->group(function () {
    Route::get('/', 'PetugasController@index')->name('petugas.index');
    Route::post('/store',  'PetugasController@store')->name('petugas.store');
    Route::post('/update/{id}',  'PetugasController@update')->name('petugas.update');
    Route::delete('/delete/{id}',  'PetugasController@destroy')->name('petugas.delete');
});
route::prefix('transaksi')->group(function () {
    Route::get('/', 'TransaksiController@index')->name('transaksi.index');
    Route::post('/tarik/{id}',  'TransaksiController@tarik')->name('transaksi.tarik');
    Route::post('/setor/{id}',  'TransaksiController@setor')->name('transaksi.setor');
    Route::delete('/bayar/{id}',  'TransaksiController@bayar')->name('transaksi.bayar');
});
route::prefix('tabungan')->group(function () {
    Route::get('/', 'TabunganController@index')->name('tabungan.index');
});

// Route::group(['middleware' => 'nasabah'], function () {
//     Route::prefix('nasabah')->group(function () {
//         Route::get('/', [NasabahController::class, 'index'])->name('nasabah.index');
//         Route::get('/kotaksaran', [NasabahController::class, 'kotaksaran'])->name('nasabah.kotaksaran');
//     });
// });
