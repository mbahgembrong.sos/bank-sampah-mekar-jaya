<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'nama'      => 'admin',
            'id_level'      => '3',
            'email'     => 'admin@gmail.com',
            'password'  => Hash::make('admin'),
            'telp'      => '08123456789',
        ]);
    }
}
