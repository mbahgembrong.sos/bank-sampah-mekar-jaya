<?php

use Illuminate\Database\Seeder;
use App\Level;
class LevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Level::create([
            'id'      => '1',
            'nama'      => 'nasabah',
        ]);
        Level::create([
            'id'      => '2',
            'nama'      => 'petugas',
        ]);
        Level::create([
            'id'      => '3',
            'nama'      => 'admin',
        ]);
    }
}
