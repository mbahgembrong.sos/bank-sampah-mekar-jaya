<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTabungansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabungans', function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table->uuid("id_user");
            $table->uuid("id_nasabah");
            $table->set('jenis_transaksi', ['setor', 'tarik', 'bayar']);
            $table->integer('saldo_tabungan');
            $table->integer('total_transaksi');
            $table->timestamps();
        });
        Schema::table('tabungans', function (Blueprint  $table) {
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('id_nasabah')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabungans');
    }
}
