<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailTabungansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_tabungans', function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table->uuid("id_transaksi");
            $table->uuid("id_sampah");
            $table->integer('jumlah_sampah');
            $table->integer('total_harga');
            $table->timestamps();
        });
        Schema::table('detail_tabungans', function (Blueprint  $table) {
            $table->foreign('id_transaksi')->references('id')->on('tabungans')->onDelete('cascade');
            $table->foreign('id_sampah')->references('id')->on('sampahs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_tabungans');
    }
}
