@extends('base.app')
@section('title', '')

@section('content')
<!-- Banner -->
<section id="banner">
			<div class="inner">
				<h2>BANK SAMPAH MEKAR JAYA</h2>
				<p>Dsn.Krajan Ds.Karangrejo Kec.Karangrejo Kab.Tulungagung<br />
					Pilah Di Rumah Bawa Ke Bank Sampah<br />
				</p>
				<ul class="actions special">
					<li><a href="{{route ('login')}}" class="button primary">Log In</a></li>
				</ul>
			</div>
			<a href="#one" class="more scrolly">Learn More</a>
		</section>

		<!-- One -->
		<section id="one" class="wrapper style1 special">
			<div class="inner">
				<header class="major">
					<h2>GERAKAN MENABUNG SAMPAH<br /></h2>
					<p>E-Sampah adalah solusi untuk menyelesaikan masalah sosial tentang kebersihan lingkungan. Nasabah dapat dengan mudah menabung sampah dari rumah tanpa kesulitan datang ke bank sampah,dengan pelayanan penjemputan.<br />
						Saldo tabungan sampah dicatat dalam aplikasi dan dapat diuangkan. Bank Sampah menjadi lebih mudah untuk menemukan pelanggan, karena mereka terintegrasi dalam manajemen aplikasi.</p>
				</header>
			</div>
		</section>

		<!-- Two -->
		<section id="two" class="wrapper alt style2">
			<section class="spotlight">
				<div class="image"><img src="{{asset('tempbase/images/pic01.jpg')}}" alt="" /></div>
				<div class="content">
					<h2>PLASTIK</h2>
					<p>Plastik memiliki efek yang merusak lingkungan karena sulit untuk membusuk secara alami. Diperkirakan, butuh sekitar 500 hingga 1.000 tahun agar plastik bisa terurai di alam.</p>
				</div>
			</section>
			<section class="spotlight">
				<div class="image"><img src="{{asset('tempbase/images/pic02.jpg')}}" alt="" /></div>
				<div class="content">
					<h2>KERTAS<br />
						</h2>
					<p>Kertas berbahan dasar kayu akan merugikan dan mempengaruhi kelestarian hutan. Jika kertas didaur ulang, setidaknya bisa menyelamatkan pohon di hutan dari penebangan.</p>
				</div>
			</section>
			<section class="spotlight">
				<div class="image"><img src="{{asset('tempbase/images/pic03.jpg')}}" alt="" /></div>
				<div class="content">
					<h2>LOGAM<br />
						</h2>
					<p>Kaleng yang dibuang di tanah akan sangat sulit terurai, sehingga memicu korosi atau karat. Korosif ini dapat mengandung beberapa logam berat yang nantinya dapat dibawa ke dalam air tanah.</p>
				</div>
			</section>
			<section class="spotlight">
				<div class="image"><img src="{{asset('tempbase/images/pic04.jpg')}}" alt="" /></div>
				<div class="content">
					<h2>KACA<br />
						</h2>
					<p>Kaca dapat didaur ulang dan menghemat sumber daya energi yang baru. Membuat produk dari kaca daur ulang menggunakan lebih sedikit bahan daripada memulai dari awal.</p>
				</div>
			</section>
		</section>

		<!-- Three -->
		<section id="three" class="wrapper style3 special">
			<div class="inner">
				<header class="major">
					<h2>BANK SAMPAH MEKAR JAYA</h2>
					<p>CARA MUDAH MENABUNG SAMPAH<br />
					</p>
				</header>
				<ul class="features">
					<li class="icon fa-paper-plane">
						<h3>Praktis</h3>
						<p>Pengelolaan Data Dilakukan Secara Digital & Dapat Meningkatkan Paperless Activity.</p>
					</li>
					<li class="icon solid fa-laptop">
						<h3>Transparan</h3>
						<p>Pengelolaan Tabungan Nasabah Dilakukan Secara Transparan.</p>
					</li>
					<li class="icon solid fa-code">
						<h3>Aman</h3>
						<p>Untuk keamanan data Anda, Bank Sampah Arum Jaya menggunakan SSL/TLS 
							Untuk Transaksi Data Dan Enkripsi Penyimpanan Data.</p>
					</li>
					<li class="icon solid fa-headphones-alt">
						<h3>Terintegrasi Secara Langsung</h3>
						<p>Data Bank Sampah Yang Sudah Terintegrasi Secara Langsung, Sehingga Memudahkan
							Proses Pemantauan Pengelolaan Sampah.</p>
					</li>
					<li class="icon fa-heart">
						<h3>Berbasis Web</h3>
						<p>Nasabah Bank Sampah Arum Jaya Dapat Melakukan Transaksi Dengan mudah Dan Nyaman.</p>
					</li>
					<li class="icon fa-flag">
						<h3>Multiplatform</h3>
						<p>Aplikasi Bank Sampah Arum Jaya Dapat Diakses Dari Semua Gadget.
							Mulai Dari Komputer, Tablet Hingga Smartphone.</p>
					</li>
				</ul>
			</div>
		</section>

		<!-- CTA -->
		<section id="cta" class="wrapper style4">
			<div class="inner">
				<header>
					<h2>Penasaran?</h2>
					<p>AYO BERGABUNG MENJADI NASABAH UNTUK GERAKAN MENABUNG SAMPAH</p>
				</header>
				<ul class="actions stacked">
					<li><a href="{{route ('login')}}" class="button fit primary">Login</a></li>
					<li><a href="https://wa.me/6285156316236" target="_blank" class="button fit">Hubungi Kami</a></li>
				</ul>
			</div>
		</section>
@endsection