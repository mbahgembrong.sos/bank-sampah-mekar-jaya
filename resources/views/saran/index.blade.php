@extends('base.app')
@section('title', '')

@section('content')
<article id="main">
    <header>
        <h2>BANK SAMPAH MEKAR JAYA</h2>
        <p>Dsn.Krajan Ds.Karangrejo Kec.Karangrejo Kab.Tulungagung<br />
            Pilah Di Rumah Bawa Ke Bank Sampah<br />
    </header>
    <section class="wrapper style5">
        <div class="inner">

            <section>
                <header>
                    <h4>Kotak Saran</h4>
                    <p>Penilaian; Kritik; Saran.</p>
                </header>
                <p>Untuk memastikan kepuasan nasabah, kami pihak bank sampah Mekar Jaya selalu terbuka 
                    akan kritik, saran, dan masukan dari nasabah kami.<br/><br/>
                </p>
            </section>

            <section>
                <h4>Form Kotak Saran</h4>
						<form method="post" action="#">
							<div class="row gtr-uniform">
								<div class="col-4 col-12-xsmall">
									<input type="text" name="demo-name" id="demo-name" value="" placeholder="Name" />
								</div>
								<div class="col-4 col-12-xsmall">
									<input type="email" name="demo-email" id="demo-email" value="" placeholder="Email" />
								</div>
                                <div class="col-4 col-12-xsmall">
									<input type="text" name="perihal" id="demo-perihal" value="" placeholder="Perihal" />
								</div>
								<div class="col-12">
									<textarea name="demo-message" id="demo-message" placeholder="Masukkan Pesan Anda" rows="6"></textarea>
								</div>
								<div class="col-12">
									<ul class="actions">
										<li><input type="submit" value="Kirim Masukan" class="primary" /></li>
										<li><input type="reset" value="Reset" /></li>
									</ul>
								</div>
							</div>
						</form>
            </section>
        </div>
    </section>
</article>
@endsection