@extends('base.app')
@section('title', '')
@section('content')
<article id="main">
    <header>
        <h2>BANK SAMPAH MEKAR JAYA</h2>
        <p>Dsn.Krajan Ds.Karangrejo Kec.Karangrejo Kab.Tulungagung<br />
            Pilah Di Rumah Bawa Ke Bank Sampah<br />
    </header>
    <section class="wrapper style5">
        <div class="inner">
            <section>
                <header>
                    <h4>Daftar Nasabah</h4>
                </header>
                {{-- <p>Untuk menjaga kepercayaan nasabah, kami menyajikan detail setiap transaksi
                    yang sudah dilakukan nasabah Mekar Jaya, dan juga Saldo terbaru masing-masing
                    Nasabah Bank Sampah Mekar Jaya.<br /><br /> --}}
                </p>
            </section>

            <section>
                <div class="table-wrapper">
                    <table class="alt" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Saldo</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php ($a=1) @endphp
                            @foreach ($nasabah as $item)
                            <tr>
                                <td>{{$a++}}</td>
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->email }}</td>
                                <td>{{ $item->getSaldo()->saldo_tabungan??0 }}</td>
                                <td>
                                    <div class="d-inline-flex ">
                                        <button class="btn btn-warning btn-xs text-white mr-2" data-toggle="modal"
                                            data-target="#modalSetor-{{ $item->id }}" data-id="{{ $item->id }}">Setor</button>
                                            @if ( ($item->getSaldo()->saldo_tabungan??0)!=0)
                                            <button class="btn btn-warning btn-xs text-white mr-2" data-toggle="modal"
                                            data-target="#modalTarik-{{ $item->id }}">Tarik</button>
                                            @endif
                                        {{-- <button class="btn btn-warning btn-xs text-white mr-2" data-toggle="modal"
                                            data-target="#modalBayar-{{ $item->id }}">Bayar</button> --}}
                                    </div>
                                    {{-- modal setor --}}
                                    <div class="modal fade" id="modalSetor-{{ $item->id }}"  tabindex="-1" role="dialog"
                                        aria-labelledby="modalSetorLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="modalSetorLabel">Setor Sampah</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{ route('transaksi.setor',$item->id)}}"
                                                        method="POST">
                                                        @csrf
                                                        <div class="form-group">
                                                            <label for="nama" class="col-form-label text-dark">Nama
                                                                :</label>
                                                            <input type="text" class="form-control" name="nama"
                                                                value="{{ $item->nama}}" readonly>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="email" class="col-form-label text-dark">Email
                                                                :</label>
                                                            <input class="form-control" type="email" name="email"
                                                                value="{{ $item->email}}" readonly></input>
                                                        </div>
                                                        <div class="form-group" id="sampah">
                                                            <label class="form-label">Sampah</label>
                                                            <div class="form-group fieldGroup" data-id="1">
                                                                <div class="input-group">
                                                                    <select name="sampah[]" class="form-control">
                                                                        <option value="" disabled selected>Pilih Sampah
                                                                        </option>
                                                                        @foreach ($sampahs as $sampah)
                                                                        <option value="{{ $sampah->id }}">{{
                                                                            $sampah->nama }} ({{ $sampah->satuan_hitung
                                                                            }})</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <input type="number" min="1" name="quantity[]"
                                                                        class="form-control quantity"
                                                                        placeholder="Masukkan quantity" />
                                                                    <div class="input-group-addon ml-3">
                                                                        <a href="javascript:void(0)"
                                                                            class="btn btn-success addMore"><i
                                                                                class="fas fa-plus"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button type="submit"
                                                            class="btn btn-primary float-right text-light">Setor</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- modal Tarik --}}
                                    <div class="modal fade" id="modalTarik-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="modalTarikLabel"
                                        aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="modalTarikLabel">Tarik Saldo</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{ route('transaksi.tarik',$item->id)}}" method="POST">
                                                        @csrf
                                                        <div class="form-group">
                                                            <label for="nama" class="col-form-label text-dark">Nama
                                                                :</label>
                                                            <input type="text" class="form-control" name="nama" value="{{ $item->nama}}" readonly>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="email" class="col-form-label text-dark">Email
                                                                :</label>
                                                            <input class="form-control" type="email" name="email" value="{{ $item->email}}" readonly></input>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="saldo" class="col-form-label text-dark">Saldo
                                                                :</label>
                                                            <input class="form-control" type="number" name="saldo" value="{{ $item->getSaldo()->saldo_tabungan??0}}" readonly></input>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="tarik" class="col-form-label text-dark">Tarik
                                                                :</label>
                                                            <input class="form-control" type="number" name="tarik" min="1"  max="{{ $item->getSaldo()->saldo_tabungan??0}}" ></input>
                                                        </div>
                                                        <button type="submit" class="btn btn-primary float-right text-light">Tarik</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </section>
</article>
{{-- modal --}}
<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAddLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalAddLabel">Tambah Sampah</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('nasabah.store')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="nama" class="col-form-label .text-dark">Nama :</label>
                        <input type="text" class="form-control" name="nama">
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-form-label .text-dark">Email :</label>
                        <input class="form-control" type="email" id="email" name="email"></input>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-form-label .text-dark">Password :</label>
                        <input class="form-control" type="password" id="password" name="password"></input>
                    </div>
                    <div class="form-group">
                        <label for="telp" class="col-form-label .text-dark">Telp :</label>
                        <input class="form-control" type="text" id="telp" name="telp"></input>
                    </div>
                    <div class="form-group">
                        <label for="alamat" class="col-form-label text-dark">Alamat</label>
                        <div class="col-sm-12">
                            <textarea id="alamat" name="alamat"></textarea>
                        </div>
                    </div>
                    <button type="submit" id="submit" class="btn btn-primary float-right text-light">Add</button>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- trash store--}}
<div class="form-group fieldGroupCopy" style="display: none;">
    <div class="input-group">
        <select name="sampah[]" class="form-control">
            <option value="" disabled selected>Pilih Sampah</option>
            @foreach ($sampahs as $sampah)
            <option value="{{ $sampah->id }}">{{ $sampah->nama }} ({{ $sampah->satuan_hitung }})</option>
            @endforeach
        </select>
        <input type="number" min="1" name="quantity[]" class="form-control quantity mr-2" placeholder="Masukkan quantity" />
        <div class="input-group-addon">
            <a href="javascript:void(0)" class="btn btn-danger remove"><i class="fas fa-trash"></i></a>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    var idUser="";
    $('.modal').on('show.bs.modal', function (event) {
        // console.log(event.relatedTarget);
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('id') // Extract info from data-* attributes
    idUser = recipient;
    })
    // input multiple field
$(document).ready(function(){
// membatasi jumlah inputan
var maxGroup = 10;

//melakukan proses multiple input
$(".addMore").click(function(){
var sampahsInput= $('body').find('.fieldGroup').length;
if(sampahsInput < maxGroup){
    var fieldHTML='<div class="form-group fieldGroup" data-id="' +(sampahsInput+1)+'">'+$('.fieldGroupCopy').html()+'</div>';
    $('body').find('#modalSetor-'+idUser+' .fieldGroup:last').after(fieldHTML);
    }else{
    alert('Maksimal '+maxGroup+' Sampah.'); } });
    //remove fields group
    $("body").on("click",".remove",function(){ $(this).parents(".fieldGroup").remove();
    });
    });
</script>
@endsection
