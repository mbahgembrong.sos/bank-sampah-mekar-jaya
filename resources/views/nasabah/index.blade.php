@extends('base.app')
@section('title', '')
@section('content')
<article id="main">
    <header>
        <h2>BANK SAMPAH MEKAR JAYA</h2>
        <p>Dsn.Krajan Ds.Karangrejo Kec.Karangrejo Kab.Tulungagung<br />
            Pilah Di Rumah Bawa Ke Bank Sampah<br />
    </header>
    <section class="wrapper style5">
        <div class="inner">
            <section>
                <header>
                    <h4>Daftar Nasabah</h4>
                </header>
                {{-- <p>Untuk menjaga kepercayaan nasabah, kami menyajikan detail setiap transaksi
                    yang sudah dilakukan nasabah Mekar Jaya, dan juga Saldo terbaru masing-masing
                    Nasabah Bank Sampah Mekar Jaya.<br /><br /> --}}
                </p>
            </section>

            <section>
                <div class="row">
                    <div class="col"></div>
                    <div class="col float-right mb-3">
                        <a data-toggle="modal" data-target="#modalAdd"
                            class="btn btn-dark btn-icon-split btn-sm float-right text-light">
                            <span class="icon text-white-50">
                                <i class="fas fa-plus"></i>
                            </span>
                            <span class="text">Tambah Nasabah</span>
                        </a>
                    </div>
                </div>
                <div class="table-wrapper">
                    <table class="alt" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Telp</th>
                                <th>Alamat</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php ($a=1) @endphp
                            @foreach ($nasabah as $item)
                            <tr>
                                <td>{{$a++}}</td>
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->email }}</td>
                                <td>{{ $item->telp }}</td>
                                <td>{{ $item->alamat }}</td>
                                <td>
                                    <div class="d-inline-flex ">
                                        <button class="btn btn-warning btn-xs text-white mr-2" data-toggle="modal"
                                            data-target="#modalEdit-{{ $item->id }}">Edit</button>
                                        <form method="POST" action="{{ route('nasabah.delete',$item->id) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-xs text-light"
                                                onclick="return confirm('Yakin ingin menghapus data ini ?')">
                                                Hapus
                                            </button>
                                        </form>
                                    </div>
                                    {{-- modal edit --}}
                                    <div class="modal fade" id="modalEdit-{{ $item->id }}" tabindex="-1" role="dialog"
                                        aria-labelledby="modalEditLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="modalEditLabel">Edit Sampah</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{ route('nasabah.update',$item->id)}}" method="POST">
                                                        @csrf
                                                        <div class="form-group">
                                                            <label for="nama" class="col-form-label text-dark">Nama
                                                                :</label>
                                                            <input type="text" class="form-control" name="nama"
                                                                value="{{ $item->nama}}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="email" class="col-form-label text-dark">Email
                                                                :</label>
                                                            <input class="form-control" type="email" name="email"
                                                                value="{{ $item->email}}"></input>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="password"
                                                                class="col-form-label text-dark">Password
                                                                :</label>
                                                            <input class="form-control" type="password" name="password"
                                                                value=""></input>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="telp" class="col-form-label text-dark">Telp
                                                                :</label>
                                                            <input class="form-control" type="text" name="telp"
                                                                value="{{ $item->telp}}"></input>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="alamat"
                                                                class="col-form-label text-dark">Alamat</label>
                                                            <div class="col-sm-12">
                                                                <textarea id="alamat"
                                                                    name="alamat">{{ $item->alamat }}</textarea>
                                                            </div>
                                                        </div>
                                                        <button type="submit" id="submit"
                                                            class="btn btn-primary float-right text-light">Edit</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </section>
</article>
{{-- modal --}}
<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAddLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalAddLabel">Tambah Sampah</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('nasabah.store')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="nama" class="col-form-label .text-dark">Nama :</label>
                        <input type="text" class="form-control" name="nama">
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-form-label .text-dark">Email :</label>
                        <input class="form-control" type="email" id="email" name="email"></input>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-form-label .text-dark">Password :</label>
                        <input class="form-control" type="password" id="password" name="password"></input>
                    </div>
                    <div class="form-group">
                        <label for="telp" class="col-form-label .text-dark">Telp :</label>
                        <input class="form-control" type="text" id="telp" name="telp"></input>
                    </div>
                    <div class="form-group">
                        <label for="alamat" class="col-form-label text-dark">Alamat</label>
                        <div class="col-sm-12">
                            <textarea id="alamat" name="alamat"></textarea>
                        </div>
                    </div>
                    <button type="submit" id="submit" class="btn btn-primary float-right text-light">Add</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
    console.log();
tinymce.init({
forced_root_block : false,
selector: 'textarea',
});
</script>
@endsection
