@extends('base.app')
@section('title', '')
@section('content')
<article id="main">
    <header>
        <h2>BANK SAMPAH MEKAR JAYA</h2>
        <p>Dsn.Krajan Ds.Karangrejo Kec.Karangrejo Kab.Tulungagung<br />
            Pilah Di Rumah Bawa Ke Bank Sampah<br />
    </header>
    <section class="wrapper style5">
        <div class="inner">

            <section>
                <header>
                    <h4>Buku Tabungan Nasabah</h4>
                    <p>Riwayat transaksi; Detail Penjualan Sampah; Saldo.</p>
                </header>
                <p>Untuk menjaga kepercayaan nasabah, kami menyajikan detail setiap transaksi
                    yang sudah dilakukan nasabah Mekar Jaya, dan juga Saldo terbaru masing-masing
                    Nasabah Bank Sampah Mekar Jaya.<br /><br />
                </p>
            </section>

            <section>
                <h4>Riwayat transaksi</h4>
                <div class="table-wrapper">
                    <table class="alt" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Jenis Transaksi</th>
                                <th>Total transaksi</th>
                                <th>Saldo</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php ($no=1) @endphp
                            @foreach ($tabungan as $item)
                            {{-- @if (Auth::user()->id_user == $item->id_nasabah) --}}
                            <tr>
                                <td>{{ $no++}}</td>
                                <td>{{ $item->created_at }}</td>
                                <td>{{ $item->jenis_transaksi }}</td>
                                <td>{{ $item->total_transaksi }}</td>
                                <td>{{ $item->saldo_tabungan }}</td>
                            </tr>
                            {{-- @endif --}}
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </section>
</article>
@endsection
