@extends('base.app')
@section('title', '')
@section('content')
<article id="main">
    <header>
        <h2>BANK SAMPAH MEKAR JAYA</h2>
        <p>Dsn.Krajan Ds.Karangrejo Kec.Karangrejo Kab.Tulungagung<br />
            Pilah Di Rumah Bawa Ke Bank Sampah<br />
    </header>
    <section class="wrapper style5">
        <div class="inner">
            <section>
                <header>
                    <h4>Daftar Sampah</h4>
                </header>
                {{-- <p>Untuk menjaga kepercayaan nasabah, kami menyajikan detail setiap transaksi
                    yang sudah dilakukan nasabah Mekar Jaya, dan juga Saldo terbaru masing-masing
                    Nasabah Bank Sampah Mekar Jaya.<br /><br /> --}}
                </p>
            </section>

            <section>
                <div class="row">
                    <div class="col"></div>
                    <div class="col float-right mb-3">
                        <a data-toggle="modal" data-target="#modalAdd"
                            class="btn btn-dark btn-icon-split btn-sm float-right text-light">
                            <span class="icon text-white-50">
                                <i class="fas fa-plus"></i>
                            </span>
                            <span class="text">Tambah Sampah</span>
                        </a>
                    </div>
                </div>
                <div class="table-wrapper">
                    <table class="alt" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Harga</th>
                                <th>Satuan Hitung</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php ($a=1) @endphp
                            @foreach ($sampah as $item)
                            <tr>
                                <td>{{$a++}}</td>
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->harga }}</td>
                                <td>{{ $item->satuan_hitung }}</td>
                                <td>
                                    <div class="d-inline-flex ">
                                        <button class="btn btn-warning btn-xs text-white mr-2" data-toggle="modal"
                                            data-target="#modalEdit-{{ $item->id }}">Edit</button>
                                        <form method="POST" action="{{ route('sampah.delete',$item->id) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-xs text-light"
                                                onclick="return confirm('Yakin ingin menghapus data ini ?')">
                                                Hapus
                                            </button>
                                        </form>
                                    </div>
                                    {{-- modal edit --}}
                                    <div class="modal fade" id="modalEdit-{{ $item->id }}" tabindex="-1" role="dialog"
                                        aria-labelledby="modalEditLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="modalEditLabel">Edit Sampah</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{ route('sampah.update',$item->id)}}" method="POST">
                                                        @csrf
                                                        <div class="form-group">
                                                            <label for="nama" class="col-form-label .text-dark">Nama
                                                                :</label>
                                                            <input type="text" class="form-control" name="nama"
                                                                value="{{ $item->nama}}" </div>
                                                            <div class="form-group">
                                                                <label for="harga"
                                                                    class="col-form-label .text-dark">Harga
                                                                    :</label>
                                                                <input class="form-control" type="number" name="harga"
                                                                    value="{{ $item->harga}}"></input>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="satuan_hitung"
                                                                    class="col-form-label .text-dark">Satuan Hitung
                                                                    :</label>
                                                                <select class="form-control" name="satuan_hitung">
                                                                    <option value="kg" @if($item->satuan_hitung ==
                                                                        'kg') selected @endif>Kg</option>
                                                                    <option value="pcs" @if($item->satuan_hitung ==
                                                                        'pcs') selected @endif>Kg</option>
                                                                    <option value="meter" @if($item->satuan_hitung
                                                                        == 'meter') selected @endif>Kg</option>
                                                                </select>
                                                            </div>
                                                            <button type="submit" id="submit"
                                                                class="btn btn-primary float-right text-light">Edit</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </section>
</article>
{{-- modal --}}
<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAddLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalAddLabel">Tambah Sampah</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('sampah.store')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="nama" class="col-form-label .text-dark">Nama :</label>
                        <input type="text" class="form-control" name="nama">
                    </div>
                    <div class="form-group">
                        <label for="harga" class="col-form-label .text-dark">Harga :</label>
                        <input class="form-control" type="number" id="harga" name="harga"></input>
                    </div>
                    <div class="form-group">
                        <label for="satuan_hitung" class="col-form-label .text-dark">Satuan Hitung :</label>
                        <select class="form-control" id="satuan_hitung" name="satuan_hitung">
                            <option value="Pilih Satuan Hitung" disabled selected></option>
                            <option value="kg">Kg</option>
                            <option value="pcs">PCS</option>
                            <option value="meter">Meter</option>
                        </select>
                    </div>
                    <button type="submit" id="submit" class="btn btn-primary float-right text-light">Add</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>

</script>
@endsection
