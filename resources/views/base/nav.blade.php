<header id="header" class="alt">
    <h1><a href="index.html">Mekar Jaya</a></h1>
    <nav id="nav">
        <ul>
            <li class="special">
                <a href="#menu" class="menuToggle"><span>Menu</span></a>
                <div id="menu">
                    <ul>
                        <li><a href="/">Home</a></li>
                        @if ( Route::current()->uri() == "/")
                        <li><a href="#two">Terkini</a></li>
                        @endif
                        @if (auth()->check())
                        @if (auth()->user()->id_level ==1)
                        <li><a href="{{route('tabungan.index')}}">Tabungan</a></li>
                        @endif
                        @if (auth()->user()->id_level ==3 ||auth()->user()->id_level ==2)
                        <li><a href="{{route('nasabah.index')}}">Nasabah</a></li>
                        <li><a href="{{route('transaksi.index')}}">Transaksi</a></li>
                        @endif
                        @if (auth()->user()->id_level ==3)
                        <li><a href="{{route('petugas.index')}}">Petugas</a></li>
                        <li><a href="{{route('sampah.index')}}">Sampah</a></li>
                        @endif
                        @endif
                        @if (Route::has('login'))
                        @auth
                        <li><a href="#" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">Log Out</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                        @else
                        <li><a href="{{route('login')}}">Log In</a></li>
                        @endauth
                        @endif
                    </ul>
                </div>
            </li>
        </ul>
    </nav>
</header>
