<!DOCTYPE HTML>
<html>

<head>
    <title>Mekar Jaya @yield('title')</title>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="{{asset('tempbase/assets/css/main.css')}}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
   <style>
       header{
           color: #ffffff;
       }
       #banner {
           color: #ffffff;
       }
       #two {
           color: #ffffff;
       }
        .modal label {
            color: #3a3a3a !important;
        }
    </style>

    @yield('head')
    <noscript>
        <link rel="stylesheet" href="{{asset('tempbase/assets/css/noscript.css')}}" />
    </noscript>
</head>

<body class="landing is-preload">

    <!-- Page Wrapper -->
    <div id="page-wrapper">

        <!-- Header -->
        <header id="header" class="alt">
            <h1><a href="index.html">Mekar Jaya</a></h1>
            @if (Route::has('login'))
            @include('base.nav')
            @endif

        </header>
        @include('sweetalert::alert')
        @yield('content')

        <!-- Footer -->
        <footer id="footer">
            <ul class="icons">
                <li><a href="#" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
                <li><a href="#" class="icon brands fa-facebook-f"><span class="label">Facebook</span></a></li>
                <li><a href="#" class="icon brands fa-instagram"><span class="label">Instagram</span></a></li>
                <li><a href="#" class="icon brands fa-dribbble"><span class="label">Dribbble</span></a></li>
                <li><a href="#" class="icon solid fa-envelope"><span class="label">Email</span></a></li>
            </ul>
            <ul class="copyright">
                <li>&copy; ApiiFajar</li>
                <li>Design: <a href="#">Ajee</a></li>
                <li>Distributed by: <a href="#">g dlu</a></li>
            </ul>
        </footer>

    </div>

    <!-- Scripts -->

    <script src="{{asset('tempbase/assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('tempbase/assets/js/jquery.scrollex.min.js')}}"></script>
    <script src="{{asset('tempbase/assets/js/jquery.scrolly.min.js')}}"></script>
    <script src="{{asset('tempbase/assets/js/browser.min.js')}}"></script>
    <script src="{{asset('tempbase/assets/js/breakpoints.min.js')}}"></script>
    <script src="{{asset('tempbase/assets/js/util.js')}}"></script>
    <script src="{{asset('tempbase/assets/js/main.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    <script src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>
    @yield('script')

</body>

</html>
