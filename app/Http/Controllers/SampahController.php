<?php

namespace App\Http\Controllers;

use App\Sampah;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class SampahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sampah = Sampah::all();
        return view('sampah.index', compact('sampah'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate(
                [
                    'nama' => 'required|min:3',
                    'harga' => 'required',
                    'satuan_hitung' => 'required',
                ]
            );

            $sampah = new Sampah;
            $sampah->nama = $request->nama;
            $sampah->harga = $request->harga;
            $sampah->satuan_hitung = $request->satuan_hitung;
            $sampah->save();
            Alert::success('Tambah Sampah', 'Data berhasil disimpan.');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert::warning('Tambah Sampah', 'Gagal tambah data.');
            return redirect()->back();
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sampah  $Sampah
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sampah $sampah,  $id)
    {
        try {
            $request->validate(
                [
                    'nama' => 'required|min:3',
                    'harga' => 'required',
                    'satuan_hitung' => 'required',
                ]
            );

            $sampah = Sampah::findOrFail($id);
            $sampah->nama = $request->nama;
            $sampah->harga = $request->harga;
            $sampah->satuan_hitung = $request->satuan_hitung;
            $sampah->update();
            Alert::success('Update Sampah', 'Data berhasil dirubah.');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert::warning('Update Sampah', 'Gagal rubah data.');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sampah  $Sampah
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sampah $sampah, $id)
    {
        try {
            $sampah = Sampah::findOrFail($id);
            $sampah->delete();
            Alert::success('Delete Sampah', 'Data berhasil dihapus');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert::warning('Delete Sampah', 'Gagal hapus data.');
            return redirect()->back();
        }
    }
}
