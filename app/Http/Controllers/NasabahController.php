<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Hash;

class NasabahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nasabah = User::where('id_level', '=', 1)->get();
        return view('nasabah.index', compact('nasabah'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate(
                [
                    'nama' => 'required|min:3',
                    'email' => 'required|unique:users',
                    'telp' => 'required|unique:users',
                    'password' => 'required',
                ]
            );

            $nasabah = new User;
            $nasabah->nama = $request->nama;
            $nasabah->email = $request->email;
            $nasabah->telp = $request->telp;
            $nasabah->id_level = 1;
            $nasabah->password = Hash::make($request->password);
            if ($request->alamat) {
                $nasabah->alamat = $request->alamat;
            }
            $nasabah->save();
            Alert::success('Tambah Nasabah', 'Data berhasil disimpan.');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert::warning('Tambah Nasabah', 'Gagal tambah data.');
            return redirect()->back();
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $Nasabah
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $nasabah,  $id)
    {
        try {
            $request->validate(
                [
                    'nama' => 'required|min:3',
                    'email' => 'required',
                    'telp' => 'required'
                ]
            );

            $nasabah = User::findOrFail($id);
            $nasabah->nama = $request->nama;
            $nasabah->email = $request->email;
            $nasabah->telp = $request->telp;
            $nasabah->id_level = 1;
            if ($request->alamat) {
                $nasabah->alamat = $request->alamat;
            }
            if ($request->password) {
                $nasabah->password = Hash::make($request->password);
            }
            $nasabah->update();
            Alert::success('Update Nasabah', 'Data berhasil dirubah.');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert::warning('Update Nasabah', 'Gagal rubah data.');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $Nasabah
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $nasabah, $id)
    {
        try {
            $nasabah = User::findOrFail($id);
            $nasabah->delete();
            Alert::success('Delete Nasabah', 'Data berhasil dihapus');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert::warning('Delete Nasabah', 'Gagal hapus data.');
            return redirect()->back();
        }
    }
}
