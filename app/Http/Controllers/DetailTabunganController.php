<?php

namespace App\Http\Controllers;

use App\DetailTabungan;
use Illuminate\Http\Request;

class DetailTabunganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DetailTabungan  $detailTabungan
     * @return \Illuminate\Http\Response
     */
    public function show(DetailTabungan $detailTabungan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DetailTabungan  $detailTabungan
     * @return \Illuminate\Http\Response
     */
    public function edit(DetailTabungan $detailTabungan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DetailTabungan  $detailTabungan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetailTabungan $detailTabungan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DetailTabungan  $detailTabungan
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetailTabungan $detailTabungan)
    {
        //
    }
}
