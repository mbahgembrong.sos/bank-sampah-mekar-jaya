<?php

namespace App\Http\Controllers;

use App\Sampah;
use App\User;
use App\Tabungan;
use App\DetailTabungan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Hash;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nasabah = User::where('id_level', '=', 1)->get();
        $sampahs = Sampah::all();
        return view('transaksi.index', compact(['nasabah', 'sampahs']));
    }
    public function setor(Request $request, $id){
        // dd($request);
        try {
            $nasabah = Tabungan::where('id_nasabah', '=', $id)->orderBy('created_at', 'DESC')->first();
            if($nasabah==null){
                $saldo = 0;
            }
            else{
            $saldo = $nasabah->saldo_tabungan;
            }
            $tabungan = new Tabungan;
            $tabungan->id_nasabah = $id;
            $tabungan->id_user= Auth::user()->id;
            $tabungan->jenis_transaksi="setor";
            $tabungan->saldo_tabungan= $saldo;
            $tabungan->total_transaksi= 0;
            $tabungan->save();
            $grandTotal=0;
        foreach ($request->sampah as $key => $value) {
            $detailtabungan = new DetailTabungan;
            $detailtabungan->id_transaksi = $tabungan->id;
            $detailtabungan->id_sampah = $value;
            $detailtabungan->jumlah_sampah = $request->quantity[$key];
            $total_harga = $request->quantity[$key] * Sampah::find($value)->harga;
            $detailtabungan->total_harga = $total_harga;
            $grandTotal
            += $total_harga;
            $detailtabungan->save();
        }
        $tabungan->total_transaksi = $grandTotal;
        $tabungan->saldo_tabungan= $saldo+$grandTotal;
        $tabungan->update();
        Alert::success('Tambah Setor', 'Data berhasil di setor');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert::warning('Tambah Setor', 'Gagal setor data.');
            return redirect()->back();
        }
    }

    public function tarik(Request $request,$id)
    {
        try {
            $nasabah = Tabungan::where('id_nasabah', '=', $id)->orderBy('created_at', 'DESC')->first();
            if ($nasabah->saldo_tabungan < $request->tarik) {
                Alert::warning('Tambah Setor', 'Gagal tarik data.');
                return redirect()->back();
            }else{
                $saldo = $nasabah->saldo_tabungan;
            }
            $tabungan = new Tabungan;
            $tabungan->id_nasabah = $id;
            $tabungan->id_user = Auth::user()->id;
            $tabungan->jenis_transaksi = "tarik";
            $tabungan->saldo_tabungan = $saldo-$request->tarik;
            $tabungan->total_transaksi = $request->tarik;
            $tabungan->save();


            Alert::success('Tambah Tarik', 'Data berhasil di tarik');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert::warning('Tambah Tarik', 'Gagal tarik data.');
            return redirect()->back();
        }
    }

}
