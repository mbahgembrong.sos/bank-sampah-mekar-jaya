<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Hash;

class PetugasController extends Controller
{
    public function index()
    {
        $petugas = User::where('id_level', '!=', 1)->get();
        return view('petugas.index', compact('petugas'));
    }


    public function store(Request $request)
    {
        try {
            $request->validate(
                [
                    'nama' => 'required|min:3',
                    'email' => 'required|unique:users',
                    'telp' => 'required|unique:users',
                    'password' => 'required',
                ]
            );

            $petugas = new User;
            $petugas->nama = $request->nama;
            $petugas->email = $request->email;
            $petugas->telp = $request->telp;
            $petugas->id_level = $request->id_level;
            $petugas->password = Hash::make($request->password);
            $petugas->save();
            Alert::success('Tambah Petugas', 'Data berhasil disimpan.');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert::warning('Tambah Petugas', 'Gagal tambah data.');
            return redirect()->back();
        }
    }

    public function update(Request $request, User $petugas,  $id)
    {
        try {
            $request->validate(
                [
                    'nama' => 'required|min:3',
                    'email' => 'required',
                    'telp' => 'required'
                ]
            );

            $petugas = User::findOrFail($id);
            $petugas->nama = $request->nama;
            $petugas->email = $request->email;
            $petugas->telp = $request->telp;
            $petugas->id_level = $request->id_level;
            if ($request->password) {
                $petugas->password = Hash::make($request->password);
            }
            $petugas->update();
            Alert::success('Update Petugas', 'Data berhasil dirubah.');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert::warning('Update Petugas', 'Gagal rubah data.');
            return redirect()->back();
        }
    }

    public function destroy(User $petugas, $id)
    {
        try {
            $petugas = User::findOrFail($id);
            $petugas->delete();
            Alert::success('Delete Petugas', 'Data berhasil dihapus');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert::warning('Delete Petugas', 'Gagal hapus data.');
            return redirect()->back();
        }
    }
}
