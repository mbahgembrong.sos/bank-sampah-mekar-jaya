<?php

namespace App\Http\Controllers;

use App\Tabungan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TabunganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tabungan = Tabungan::where('id_nasabah', '=',Auth::user()->id)->orderBy('created_at', 'DESC')->get();
        return view('tabungan.index', compact(['tabungan']));
    }
}
