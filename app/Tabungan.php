<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Exception\UnableToBuildUuidException;
use Ramsey\Uuid\Uuid as Generator;

class Tabungan extends Model
{
    // use HasFactory;
    protected $primaryKey = 'id';
    public $incrementing = false;
    // protected $dates = ['tgl_kirim', 'tgl_surat'];
    // protected $datimes = ['waktu'];

    protected $fillable = ['id', 'id_user', 'id_nasabah', 'jenis_transaksi','saldo_tabungan','total_transaksi'];
    public function getSaldo()
    {
        return Tabungan::where('id_nasabah', '=', $this->id_nasabah)->orderBy('created_at', 'DESC')->first()->saldo_tabungan;
    }
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->id = Generator::uuid4()->toString();
            } catch (UnableToBuildUuidException $e) {
                abort(500, $e->getMessage());
            }
        });
    }
}
