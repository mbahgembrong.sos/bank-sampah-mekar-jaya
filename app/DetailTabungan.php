<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Exception\UnableToBuildUuidException;
use Ramsey\Uuid\Uuid as Generator;

class DetailTabungan extends Model
{
    // use HasFactory;
    protected $primaryKey = 'id';
    public $incrementing = false;
    // protected $dates = ['tgl_kirim', 'tgl_surat'];
    // protected $datimes = ['waktu'];

    protected $fillable = ['id', 'id_transaksi', 'id_sampah', 'jumlah_sampah', 'total_harga'];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->id = Generator::uuid4()->toString();
            } catch (UnableToBuildUuidException $e) {
                abort(500, $e->getMessage());
            }
        });
    }
}
